//Includes
//Include AMS_Event
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/plugin/Plugin.h"

//Include AMS_Event Events
#include "game_plugin\event\MedicalTestEvent.h"
#include "game_plugin\event\MedicalTestEventFactory.h"

//Include em5
#include <em5/plugin/version/PluginVersion.h>
#include "em5\reflection\CampDefines.h"

//Include qsf
#include <qsf/localization/LocalizationSystem.h>
#include <qsf/map/Entity.h>

//Include qsf_game
#include <qsf_game/command/CommandManager.h>

//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
namespace AMS_Event
{


	//[-------------------------------------------------------]
	//[ Public methods                                        ]
	//[-------------------------------------------------------]
	Plugin::Plugin() :
		qsf::Plugin(new em5::PluginVersion())
	{
		// Nothing to do in here
	}


	//[-------------------------------------------------------]
	//[ Protected virtual qsf::Plugin methods                 ]
	//[-------------------------------------------------------]
	bool Plugin::onInstall()
	{
		try
		{
		
			//Event

			QSF_START_CAMP_CLASS_EXPORT(AMS_Event::MedicalTestEventFactory, "MedicalTestEventFactory", "Medical Test Event Factory")
				EM5_CAMP_IS_FREEPLAY_EVENT_FACTORY
				QSF_ADD_CAMP_PROPERTY(Age, AMS_Event::MedicalTestEventFactory::getAgeAsString, AMS_Event::MedicalTestEventFactory::setAgeAsString, "Age", "all")
				QSF_ADD_CAMP_PROPERTY(Weather, AMS_Event::MedicalTestEventFactory::getWeatherAsString, AMS_Event::MedicalTestEventFactory::setWeatherAsString, "Weather", "all")
				QSF_ADD_CAMP_PROPERTY(Inside, AMS_Event::MedicalTestEventFactory::getInsideBuilding, AMS_Event::MedicalTestEventFactory::setInsideBuilding, "Inside", false)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS_Event::MedicalTestEvent, "MedicalCheerEvent", "Medical cheer event")
				EM5_CAMP_IS_FREEPLAY_EVENT
				QSF_ADD_CAMP_PROPERTY(Injury, AMS_Event::MedicalTestEvent::getInjuryName, AMS_Event::MedicalTestEvent::setInjuryName, "Injury", "")
				QSF_ADD_CAMP_PROPERTY(RunningDelay, AMS_Event::MedicalTestEvent::getRunningDelay, AMS_Event::MedicalTestEvent::setRunningDelay, "RunningDelay", false)
				QSF_ADD_CAMP_PROPERTY(SecondEventTag, AMS_Event::MedicalTestEvent::getSecondEventTag, AMS_Event::MedicalTestEvent::setSecondEventTag, "SecondEventTag", "")
			QSF_END_CAMP_CLASS_EXPORT

			// Done
			return true;
		}
		catch (const std::exception& e)
		{
			// Error!
			QSF_ERROR("Failed to install the plugin '" << getName() << "'. Exception caught: " << e.what(), QSF_REACT_NONE);
			return false;
		}
	}

	bool Plugin::onStartup()
	{
		// Nothing to do in here

		// Done
		return true;
	}

	void Plugin::onShutdown()
	{
		// Nothing to do in here
	}

	void Plugin::onUninstall()
	{
		// Removing classes is not possible within the CAMP reflection system

		// Nothing to do in here
	}


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
} // AMS_Event
