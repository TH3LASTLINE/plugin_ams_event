//Header guard
#pragma once

//Includes
//Include em5
#include "em5\freeplay\event\FreeplayEvent.h"

//Include qsf
#include "qsf\message\MessageProxy.h"
#include "qsf\time\Time.h"

//Namespace AMS_Event start
namespace AMS_Event
{

	//Classes
	
	class MedicalTestEvent : public em5::FreeplayEvent
	{

		//public definitions
		public:
			
			static const uint32 FREEPLAY_EVENT_ID;

		//public methods
		public:

			MedicalTestEvent();

			virtual ~MedicalTestEvent();

			uint64 getTargetPersonId() const;

			void setTargetPerson(qsf::Entity& entity);

			const std::string& getInjuryName() const;

			void setInjuryName(const std::string& injuryName);

			float getRunningDelay() const;

			void setRunningDelay(float runningDelay);

			QSF_DEFINE_SIMPLE_GETTER_AND_SETTER(EventLayer, std::string, mEventLayer);
			QSF_DEFINE_SIMPLE_GETTER_AND_SETTER(RescueWithHelicopter, bool, mRescueWithHelicopter);
			QSF_DEFINE_SIMPLE_GETTER_AND_SETTER(SecondEventTag, const std::string&, mSecondEventTag);

		//Public virtual em5::FreeplayEvent methods
		public:

			virtual bool onStartup() override;
			virtual void onShutdown() override;
			virtual void onRun() override;
			virtual bool onFailure(EventResult& eventResult) override;
			virtual void updateFreeplayEvent(const qsf::Time& timePassed) override;
			virtual bool addEntityToEvent(qsf::Entity& targetEntity, em5::eventspreadreason::Reason eventSpreadReason, bool newReason) override;
			virtual void hintCallback(em5::Observer& hintObserver) override;
			virtual const qsf::Entity* getFocusEntity() override;
			virtual bool checkEventConfiguration() override;
			virtual void serialize(qsf::BinarySerializer& serializer) override;


		//Private methods
		private:

			void startObjectives(const qsf::Entity& targetEntity);
			void startHintObservers(const qsf::Entity& targetEntity);
			bool checkCandidate(qsf::Entity* targetEntity);
			void delayedStartup(const qsf::MessageParameters& parameters);
			void activateLayer(const std::string& layerName);
			void deactivateLayer(const std::string& layerName);
			int MedicalTestEvent::SetInjuryLevel(std::string Injury);
			std::string MedicalTestEvent::setEventNamefromRandom(std::string Injury);

		//Private data
		private:

			uint64 mInjuredEntityId;
			uint64 mSpawnPointEntityId;

			qsf::NamedIdentifier mInjuryName;

			qsf::Time mRunningDelay;

			std::string mHintLowLifeEnergyName;
			std::string mHintLowHealthEnergyName;
			std::string mEventLayer;
			std::string mSecondEventTag;

			bool mRescueWithHelicopter;

			qsf::MessageProxy mMessageProxy;
			qsf::MessageProxy mTreatedPersonMessageProxy;

		//CAMP reflection system
			QSF_CAMP_RTTI()

	};

}//namespace AMS_Event end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS_Event::MedicalTestEvent)