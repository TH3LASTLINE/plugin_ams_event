//Header guard
#pragma once

//Includes
//Include em5
#include "em5\freeplay\factory\FreeplayEventFactory.h"
#include "em5\component\person\PersonComponent.h"
#include "em5\environment\weather\WeatherComponent.h"

//Namespace AMS_Event start
namespace AMS_Event
{

	//Classes
	class MedicalTestEventFactory : public em5::FreeplayEventFactory
	{

		//Public definitions
		public:

			typedef qsf::EnumBitmask<uint8, em5::PersonComponent::Age> AgeBitmask;

		//public methods
		public:

			MedicalTestEventFactory();

			virtual ~MedicalTestEventFactory();

			std::string getAgeAsString() const;

			void setAgeAsString(const std::string& jsonAge);

			inline AgeBitmask getAge() const;

			inline void setAge(AgeBitmask age);

			std::string getWeatherAsString() const;

			void setWeatherAsString(const std::string& weather);

			inline em5::WeatherComponent::WeatherBitmask getWeather() const;

			inline void setWeather(em5::WeatherComponent::WeatherBitmask weather);

			inline bool getInsideBuilding() const;

			inline void setInsideBuilding(bool insideBuilding);

		//Public virtual em5::FreeplayEventFactory methods
		public:

			virtual em5::FreeplayEvent* tryTriggerEvent() override;

		//Protected methods
		protected:

			bool checkWeatherConditions() const;

			qsf::Entity* findCandidate() const;

		//Private methods
		private:

			bool checkCandidate(qsf::Entity& candidateEntity) const;

		//private data
		private:
			
			AgeBitmask mAgeBitmask;

			em5::WeatherComponent::WeatherBitmask mWeatherBitmask;

			bool mInsideBuilding;

			std::string mEventLayer;
	
		//CAMP reflection system

			QSF_CAMP_RTTI()

	};

}//namespace AMS_Event end

//Implementation
#include "game_plugin\event\MedicalTestEventFactory-inl.h"

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS_Event::MedicalTestEventFactory)