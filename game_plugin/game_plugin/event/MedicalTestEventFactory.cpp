//Includes
//Include AMS_Event
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\event\MedicalTestEvent.h"
#include "game_plugin\event\MedicalTestEventFactory.h"

//Include em5
#include "em5\component\event\EventIdComponent.h"
#include "em5\event\EventHelper.h"
#include "em5\map\EntityHelper.h"

//Include qsf
#include "qsf\QsfHelper.h"
#include "qsf\map\Map.h"
#include "qsf\map\Entity.h"
#include "qsf\map\query\ComponentMapQuery.h"
#include "qsf\math\Random.h"
#include "qsf\reflection\CampHelper.h"

//Namespace AMS_Event start
namespace AMS_Event
{

	//Public methods

	MedicalTestEventFactory::MedicalTestEventFactory() :
		mInsideBuilding(false)
	{

	}

	MedicalTestEventFactory::~MedicalTestEventFactory()
	{

	}

	std::string MedicalTestEventFactory::getAgeAsString() const
	{
		return qsf::CampHelper::enumBitmaskToString(mAgeBitmask);
	}

	void MedicalTestEventFactory::setAgeAsString(const std::string& jsonAge)
	{
		qsf::CampHelper::enumBitmaskFromString(jsonAge, mAgeBitmask);
	}

	std::string MedicalTestEventFactory::getWeatherAsString() const
	{
		return qsf::CampHelper::enumBitmaskToString(mWeatherBitmask);
	}

	void MedicalTestEventFactory::setWeatherAsString(const std::string& jsonWeather)
	{
		qsf::CampHelper::enumBitmaskFromString(jsonWeather, mWeatherBitmask);
	}

	//Public virtual em5::FreeplayEventFactory methods

	em5::FreeplayEvent* MedicalTestEventFactory::tryTriggerEvent()
	{

		if (!checkWeatherConditions())
		{
		//	setTriggerFailReasons("Wrong Weather Condition");

			return nullptr;
		}

		qsf::Entity* targetEntity = nullptr;
		qsf::Entity* spawnPointEntity = nullptr;

		if (!getEventTag().empty())
		{
			spawnPointEntity = em5::EventHelper::findSpawnPoint(qsf::StringHash(getEventTag()));
			if (nullptr == spawnPointEntity)
			{
				setTriggerFailReason("Could not find spawn point by event tag " + getEventTag());

				return nullptr;
			}

			targetEntity = em5::EventHelper::spawnEntity(*spawnPointEntity);

			if (nullptr == targetEntity)
			{
				setTriggerFailReason("Could not find spawn entity from spawn point with event tag " + getEventTag());

				return nullptr;
			}

		}
		else
		{
			targetEntity = findCandidate();
			if (nullptr == targetEntity)
			{
				setTriggerFailReason("No valid victim candidate found");

				return nullptr;
			}
		}

		MedicalTestEvent& freeplayEvent = createAndInitializeEvent<MedicalTestEvent>();

		deserializeRandomFreeplayVariant(freeplayEvent);

		freeplayEvent.setTargetPerson(*targetEntity);

		return &freeplayEvent;
	}

	//Protected methods

	bool MedicalTestEventFactory::checkWeatherConditions() const
	{
		return true;
	}

	qsf::Entity* MedicalTestEventFactory::findCandidate() const
	{
		const qsf::ComponentCollection::ComponentList<em5::PersonComponent>& personComponentsInMap = qsf::ComponentMapQuery(QSF_MAINMAP).getAllInstances<em5::PersonComponent>();

		if (personComponentsInMap.empty())
		{
			return nullptr;
		}

		std::vector<em5::PersonComponent*> openPersonComponents = personComponentsInMap.copyToVector();

		const size_t numberOfPersonInMap = openPersonComponents.size();
		const size_t totalTries = 40;

		for (size_t tries = 0; tries < numberOfPersonInMap; ++tries)
		{
			qsf::Entity& entity = qsf::Random::getPickOutAnyOf(openPersonComponents)->getEntity();

			if (checkCandidate(entity))
			{
				return &entity;
			}

			if (tries > totalTries)
				return nullptr;
		}

		return nullptr;
	}

	//Private methods

	bool MedicalTestEventFactory::checkCandidate(qsf::Entity& entity) const
	{
		em5::EntityHelper entityHelper(entity);

		if (!entityHelper.isEntityValidEventTarget())
			return false;

		if (!entityHelper.isCivilPerson())
			return false;

		if (entityHelper.isPersonInjured())
			return false;

		if (!mAgeBitmask.isSet(entity.getComponentSafe<em5::PersonComponent>().getAge()))
			return false;

		if (mInsideBuilding)
		{
			if (!entityHelper.isEntityInBuilding())
				return false;
		}

		else
		{
			if (nullptr != entityHelper.getContainerEntity())
				return false;

			if (entityHelper.isEntityHardLinked())
				return false;
		}

		return true;
	}

} //Namespace AMS_Event end