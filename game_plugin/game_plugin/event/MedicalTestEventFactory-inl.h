//Namespace AMS_Event start
namespace AMS_Event
{

	//public methods

	inline MedicalTestEventFactory::AgeBitmask MedicalTestEventFactory::getAge() const
	{
		return mAgeBitmask;
	}

	inline void MedicalTestEventFactory::setAge(MedicalTestEventFactory::AgeBitmask age)
	{
		mAgeBitmask = age;
	}

	inline em5::WeatherComponent::WeatherBitmask MedicalTestEventFactory::getWeather() const
	{
		return mWeatherBitmask;
	}

	inline void MedicalTestEventFactory::setWeather(em5::WeatherComponent::WeatherBitmask weather)
	{
		mWeatherBitmask = weather;
	}

	inline bool MedicalTestEventFactory::getInsideBuilding() const
	{
		return mInsideBuilding;
	}

	inline void MedicalTestEventFactory::setInsideBuilding(bool insideBuilding)
	{
		mInsideBuilding = insideBuilding;
	}

}//Namespace AMS_Event end