//Includes
//Include AMS_Event
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\event\MedicalTestEvent.h"

//Include em5
#include "em5\event\EventHelper.h"
#include "em5\component\event\EventIdComponent.h"
#include "em5\component\spawnpoint\SpawnPointComponent.h"
#include "em5\freeplay\objective\ObjectiveHelper.h"
#include "em5\freeplay\event\FreeplayEvent.h"
#include "em5\map\MapHelper.h"
#include "em5\map\EntityHelper.h"
#include "em5\game\Game.h"
#include "em5\health\HealthHelper.h"
#include "em5\health\HealthSystem.h"
#include "em5\health\HealthComponent.h"
#include "em5\logic\ObserverHelper.h"
#include "em5\logic\HintHelper.h"
#include "em5\logic\observer\PersonDiedObserver.h"
#include "em5\logic\observer\TreatPersonObserver.h"
#include "em5\logic\observer\hint\HintLowEnergyObserver.h"
#include "em5\plugin\Messages.h"
#include "em5\EM5Helper.h"

//Include qsf_game
#include "qsf_game\component\event\EventTagManagerComponent.h"
#include "qsf_game\timer\GameTimerManager.h"

//Include qsf_compositing
#include "qsf_compositing\component\TintableMeshComponent.h"

//Include qsf
#include "qsf\math\Random.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\logic\action\WaitAction.h"
#include "qsf\log\LogSystem.h"
#include "qsf\audio\SoundTrackManager.h"
#include "qsf\audio\component\DynamicMusicCompositorComponent.h"
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\serialization\binary\BasicTypeSerialization.h"
#include "qsf\serialization\binary\StlTypeSerialization.h"
#include "qsf\QsfHelper.h"
#include "qsf\localization\LocalizationSystem.h"

//Namespace AMS_Event start
namespace AMS_Event
{

	//Local helper funktions
	//Namespace detail start
	namespace detail
	{

		inline qsf::Entity* getOrSpawnEntity(qsf::Entity& entity)
		{
			em5::SpawnPointComponent* spawnPointComponent = entity.getComponent<em5::SpawnPointComponent>();

			if (nullptr != spawnPointComponent)
			{
				qsf::Entity* spawnedEntity = spawnedEntity = spawnPointComponent->spawnRandom();
				if (nullptr != spawnedEntity)
				{

					qsf::compositing::TintableMeshComponent* tintableMeshComponent = spawnedEntity->getComponent<qsf::compositing::TintableMeshComponent>();
					if (nullptr != tintableMeshComponent)
					{
						tintableMeshComponent->setTintPaletteIndex(qsf::Random::getRandomInt(1, 15));
					}
				}
				return spawnedEntity;
			}
			else
			{
				return &entity;
			}
		}

	} //namespace detail end

	//public definitions

	const uint32 MedicalTestEvent::FREEPLAY_EVENT_ID = qsf::StringHash("AMS_Event::MedicalTestEvent");

	//public methods

	MedicalTestEvent::MedicalTestEvent() :
		mInjuredEntityId(qsf::getUninitialized<uint64>()),
		mRescueWithHelicopter(false),
		mSpawnPointEntityId(qsf::getUninitialized<uint64>())
	{

	}

	MedicalTestEvent::~MedicalTestEvent()
	{

	}

	uint64 MedicalTestEvent::getTargetPersonId() const
	{
		return mInjuredEntityId;
	}

	void MedicalTestEvent::setTargetPerson(qsf::Entity& entity)
	{
		mInjuredEntityId = entity.getId();
	}

	const std::string& MedicalTestEvent::getInjuryName() const
	{
		return mInjuryName;
	}

	void MedicalTestEvent::setInjuryName(const std::string& injuryName)
	{
		mInjuryName = injuryName;
	}

	float MedicalTestEvent::getRunningDelay() const
	{
		return mRunningDelay.getSeconds();
	}

	void MedicalTestEvent::setRunningDelay(float runningDelay)
	{
		mRunningDelay = qsf::Time::fromSeconds(runningDelay);
	}

	//Public virtual em5::FreeplayEvent methods

	bool MedicalTestEvent::onStartup()
	{
		if (0.0f == mRunningDelay.getSeconds())
		{
			setRunning();
		}
		else
		{
			qsf::Entity* personEntity = getMap().getEntityById(getTargetPersonId());
			em5::EventIdComponent* eventIdComponent = personEntity->getOrCreateComponent<em5::EventIdComponent>();

			eventIdComponent->setEvent(*this, em5::eventspreadreason::NO_REASON);

			qsf::MessageConfiguration message(em5::Messages::EM5_EVENT_TIMER_SIGNAL, getId());

			mMessageProxy.registerAt(message, boost::bind(&MedicalTestEvent::delayedStartup, this, _1));

			EM5_GAMETIMERS.addTimer(message, mRunningDelay);
		}

		return true;
	}

	void MedicalTestEvent::onShutdown()
	{
		qsf::Entity* entity = getMap().getEntityById(mSpawnPointEntityId);

		if (nullptr != entity)
		{
			entity->destroyComponent<em5::EventIdComponent>();
		}

		if (!mEventLayer.empty())
		{
			deactivateLayer(mEventLayer);
		}
	}

	void MedicalTestEvent::onRun()
	{
		qsf::Entity* personEntity = getMap().getEntityById(mInjuredEntityId);
		em5::HealthComponent* healthComponent = personEntity->getComponent<em5::HealthComponent>();
		
		int mRandom = qsf::Random::getRandomInt(0, 40);

		if (mRandom == 0) mInjuryName = "CirculatoryCollapse";
		else if (mRandom == 1) mInjuryName = "CirculatoryCollapse";
		else if (mRandom == 2) mInjuryName = "Broken_Arm";
		else if (mRandom == 3) mInjuryName = "Broken_Leg";
		else if (mRandom == 4) mInjuryName = "Broken_Skull";
		else if (mRandom == 5) mInjuryName = "Brain_Bleeding";
		else if (mRandom == 6) mInjuryName = "Inner_Bleeding";
		else if (mRandom == 7) mInjuryName = "Lost_Arm";
		else if (mRandom == 8) mInjuryName = "Lost_Leg";
		else if (mRandom == 9) mInjuryName = "Whiplash";
		else if (mRandom == 10) mInjuryName = "stab_wound_body";
		else if (mRandom == 11) mInjuryName = "stab_wound_body_inner_bleeding";
		else if (mRandom == 12) mInjuryName = "bite_wound";
		else if (mRandom == 13) mInjuryName = "bite_wound_bleeding";
		else if (mRandom == 14) mInjuryName = "Head_body_laceration";
		else if (mRandom == 15) mInjuryName = "electric_shock";
		else if (mRandom == 16) mInjuryName = "Shock";
		else if (mRandom == 17) mInjuryName = "Weakness";
		else if (mRandom == 18) mInjuryName = "Water_Filled_Lung";
		else if (mRandom == 19) mInjuryName = "HeartAttack";
		else if (mRandom == 20) mInjuryName = "Stroke";
		else if (mRandom == 21) mInjuryName = "CirculatoryCollapse";
		else if (mRandom == 22) mInjuryName = "Food_Poisoning";
		else if (mRandom == 23) mInjuryName = "Alcohol_Poisoning";
		else if (mRandom == 24) mInjuryName = "Low_Blood_Pressure";
		else if (mRandom == 25) mInjuryName = "Angina_Pectoris";
		else if (mRandom == 26) mInjuryName = "Shortage_Of_Breath";
		else if (mRandom == 27) mInjuryName = "Collapsed_lung";
		else if (mRandom == 28) mInjuryName = "Hypothermia";
		else if (mRandom == 29) mInjuryName = "Avian_Influenza";
		else if (mRandom == 30) mInjuryName = "Avian_Influenza_Injured";
		else if (mRandom == 31) mInjuryName = "Swine_Influenza";
		else if (mRandom == 32) mInjuryName = "Swine_Influenza_Injured";
		else if (mRandom == 33) mInjuryName = "Hard_Asthma";
		else if (mRandom == 34) mInjuryName = "Hard_Asthma_Injured";
		else if (mRandom == 35) mInjuryName = "Hydrophobia";
		else if (mRandom == 36) mInjuryName = "Hydrophobia_Injured";
		else if (mRandom == 37) mInjuryName = "CirculatoryCollapse";
		else if (mRandom == 38) mInjuryName = "CirculatoryCollapse";
		else if (mRandom == 39) mInjuryName = "Alcohol_Poisoning";
		else if (mRandom == 40) mInjuryName = "Alcohol_Poisoning";

		//!
		std::string mEventName = MedicalTestEvent::setEventNamefromRandom(mInjuryName);
		em5::FreeplayEvent::setEventName(mEventName);  //!

		healthComponent->injurePersonByEventById(mInjuryName, this);

		startObjectives(*personEntity);
		startHintObservers(*personEntity);

		em5::HintHelper::showSupervisorMessage(em5::HintHelper::getRandomStringOfIdString(em5::HintHelper(*this).getHintParameters().mSupervisorMessageIds));

		createEventLocationEntity(*personEntity);
	}

	bool MedicalTestEvent::onFailure(EventResult& eventResult)
	{
		return FreeplayEvent::onFailure(eventResult);
	}

	void MedicalTestEvent::updateFreeplayEvent(const qsf::Time& timePassed)
	{
		checkObjectivesState();
	}

	bool MedicalTestEvent::addEntityToEvent(qsf::Entity& targetEntity, em5::eventspreadreason::Reason eventSpreadReason, bool newReason)
	{
		em5::ObserverHelper observerHelper(*this);

		if (em5::eventspreadreason::INJURY == eventSpreadReason)
		{
			em5::Objective& failConditionPersonDiedObjective = em5::ObjectiveHelper(*this).getOrCreateObjectiveByTypeId(em5::ObjectiveHelper::OBJECTIVE_FAIL_DEADPERSONS);
			em5::PersonDiedObserver& observerDied = observerHelper.createObserver<em5::PersonDiedObserver>(targetEntity.getId());

			observerDied.connectToObjective(failConditionPersonDiedObjective);
		}

		return FreeplayEvent::addEntityToEvent(targetEntity, eventSpreadReason, newReason);
	}

	void MedicalTestEvent::hintCallback(em5::Observer& hintObserver)
	{
		em5::HintHelper::showHint(hintObserver.getName());

		if (mHintLowLifeEnergyName == hintObserver.getName() && getDynamicMusicCompositor().getMusicLevel() != 3)
		{
			getDynamicMusicCompositor().changeMusicTrack();
			getDynamicMusicCompositor().setMusicLevel(3);
		}

		if (mHintLowHealthEnergyName == hintObserver.getName() && getDynamicMusicCompositor().getMusicLevel() != 3)
		{
			getDynamicMusicCompositor().changeMusicTrack();
			getDynamicMusicCompositor().setMusicLevel(3);
		}
	}

	const qsf::Entity* MedicalTestEvent::getFocusEntity()
	{
		{
			qsf::Entity* candidateEntity = getMap().getEntityById(mInjuredEntityId);

			if (nullptr != candidateEntity && em5::EntityHelper(*candidateEntity).isPersonInjured())
			{
				return candidateEntity;
			}
		}

		em5::ObjectiveHelper objectiveHelper(*this);

		{
			const em5::Objective* treatPersons = objectiveHelper.getObjectiveByTypeId(em5::ObjectiveHelper::OBJECTIVE_NEED_TREATPERSONS);

			if (nullptr != treatPersons)
			{
				const qsf::Entity* candidateEntity = getMap().getEntityById(treatPersons->getRandomNeededEntityId());

				if (nullptr != candidateEntity)
				{
					return candidateEntity;
				}
			}
		}

		{
			const em5::Objective* healPersons = objectiveHelper.getObjectiveByTypeId(em5::ObjectiveHelper::OBJECTIVE_NEED_HEALPERSONS);

			if (nullptr != healPersons)
			{
				const qsf::Entity* candidateEntity = getMap().getEntityById(healPersons->getRandomNeededEntityId());

				if (nullptr != candidateEntity)
				{
					return candidateEntity;
				}
			}
		}

		return getEventLocationEntity();
	}

	bool MedicalTestEvent::checkEventConfiguration()
	{
		if (nullptr == EM5_HEALTH.getInjuryById(mInjuryName))
		{
			return false;
		}

		return true;
	}

	void MedicalTestEvent::serialize(qsf::BinarySerializer& serializer)
	{
		FreeplayEvent::serialize(serializer);

		serializer.serialize(mInjuredEntityId);
		serializer.serialize(mInjuryName);
		serializer.serialize(mEventLayer);
	}

	//Private methods

	void MedicalTestEvent::startObjectives(const qsf::Entity& targetEntity)
	{
		em5::Objective& failConidtionPersonDiedObjective = em5::ObjectiveHelper(*this).getOrCreateObjectiveByTypeId(em5::ObjectiveHelper::OBJECTIVE_FAIL_DEADPERSONS);

		failConidtionPersonDiedObjective.setNeededNumber(1);
	}

	void MedicalTestEvent::startHintObservers(const qsf::Entity& targetEntity)
	{
		const em5::HintHelper hintHelper(*this);

		hintHelper.createAndInitializeGeneralHintsAtInjured();

		if (hintHelper.getHintParameters().mHintsAtLowLifeEnergy != "off")
		{
			float hintLowLifeThreshold = hintHelper.getHintParameters().mHintsAtLowLifeEnergy_Threshold;

			mHintLowLifeEnergyName = em5::HintHelper::getRandomStringOfIdString(hintHelper.getHintParameters().mHintsAtLowLifeEnergy);

			createObserver<em5::HintLowEnergyObserver>(mInjuredEntityId, mHintLowLifeEnergyName).initialize(hintLowLifeThreshold, em5::HintLowEnergyObserver::HINTENERGYTYPE_LIFE);
		}

		if (hintHelper.getHintParameters().mHintsAtLowHealthEnergy != "off")
		{
			float hintLowHealthThreshold = hintHelper.getHintParameters().mHintsAtLowHealthEnergy_Threshold;
			
			mHintLowHealthEnergyName = em5::HintHelper::getRandomStringOfIdString(hintHelper.getHintParameters().mHintsAtLowHealthEnergy);

			createObserver<em5::HintLowEnergyObserver>(mInjuredEntityId, mHintLowHealthEnergyName).initialize(hintLowHealthThreshold, em5::HintLowEnergyObserver::HINTENERGYTYPE_HEALTH);
		}
	}

	bool MedicalTestEvent::checkCandidate(qsf::Entity* targetEntity)
	{
		if (nullptr == targetEntity)
			return false;

		em5::EntityHelper entityHelper(*targetEntity);

		if (!entityHelper.isCivilPerson())
			return false;

		if (entityHelper.isEntityValidEventTarget())
			return false;

		if (entityHelper.isEntityHardLinked())
			return false;

		return true;
	}

	void MedicalTestEvent::delayedStartup(const qsf::MessageParameters& parameters)
	{
		qsf::Entity* personEntity = getMap().getEntityById(getTargetPersonId());

		if (checkCandidate(personEntity))
		{
			personEntity->destroyComponent<em5::EventIdComponent>();

			setRunning();
		}
		else
		{
			abortEvent();
		}
	}

	void MedicalTestEvent::activateLayer(const std::string& layerName)
	{
		em5::MapHelper(getMap()).activateLayerByName(layerName);
	}

	void MedicalTestEvent::deactivateLayer(const std::string& layerName)
	{
		em5::MapHelper(getMap()).deactivateLayerByName(layerName);
	}



	int MedicalTestEvent::SetInjuryLevel(std::string Injury)	//All Injuries of the vanilla game
	{
		int mInjuryLevel;
		if (Injury == "Burn_3_Grade") mInjuryLevel = 8;
		else if (Injury == "Burn_4_Grade") mInjuryLevel = 8;
		else if (Injury == "Burn_Of_Breath") mInjuryLevel = 3;    // Verbrannte Atemwege
		else if (Injury == "Smoke_Poisoning") mInjuryLevel = 3;   // "Rauchgasvergiftung"
		else if (Injury == "Broken_Arm") mInjuryLevel = 2;   // "Knochenbruch Arm"
		else if (Injury == "Broken_Leg") mInjuryLevel = 2;  // "Knochenbruch Bein"	
		else if (Injury == "Broken_Skull") mInjuryLevel = 0;  // "Schädelbasisbruch"
		else if (Injury == "Brain_Bleeding") mInjuryLevel = 0;   // "Hirnblutung"
		else if (Injury == "Inner_Bleeding") mInjuryLevel = 0;  // "Innere Blutungen"
		else if (Injury == "Lost_Arm") mInjuryLevel = 5;   // "Arm amputiert"
		else if (Injury == "Lost_Leg") mInjuryLevel = 5;   // "Bein amputiert"
		else if (Injury == "Whiplash") mInjuryLevel = 6;   // "Schleudertrauma"
		else if (Injury == "gun_shot_wound_lung") mInjuryLevel = 1;   // "Offene Schusswunde, Lungenflügel perforiert"
		else if (Injury == "gun_shot_wound_Body") mInjuryLevel = 1;   // "Durchschuss (Rumpf), stark blutend"
		else if (Injury == "gun_shot_wound_Body_hard") mInjuryLevel = 1;   // "Durchschuss (Rumpf), innere Blutungen"
		else if (Injury == "stab_wound_lung") mInjuryLevel = 1;   // "Offene Stichwunde, Lungenflügel perforiert"
		else if (Injury == "stab_wound_body") mInjuryLevel = 1;   // "Offene Stichwunde (Rumpf), stark blutend"
		else if (Injury == "stab_wound_body_inner_bleeding") mInjuryLevel = 1;  // "Offene Stichwunde, innere Blutungen"
		else if (Injury == "bite_wound") mInjuryLevel = 1;  // "Offene Bisswunden, Muskel- und Nervengewebe verletzt"
		else if (Injury == "bite_wound_bleeding") mInjuryLevel = 1;  // "Offene Bisswunden, Arterie verletzt"
		else if (Injury == "Head_body_laceration") mInjuryLevel = 1;	//	"Platzwunden an Körper und Kopf"
		else if (Injury == "electric_shock") mInjuryLevel = 0;  // "Lähmung des Herzens als Folge eines schweren elektrischen Schlags"
		else if (Injury == "Shock") mInjuryLevel = 0;	//	"Schockzustand (nach Schaltkasten oftmals...)"
		else if (Injury == "Weakness") mInjuryLevel = 6;  // "Entkräftung"
		else if (Injury == "Water_Filled_Lung") mInjuryLevel = 0;  // "Wasser in der Lunge, Atemstillstand"
		else if (Injury == "HeartAttack") mInjuryLevel = 4;  // "Herzinfarkt"
		else if (Injury == "Stroke") mInjuryLevel = 6;  // "Schlaganfall"
		else if (Injury == "CirculatoryCollapse") mInjuryLevel = 6;  // "Kreislaufkollaps"
		else if (Injury == "Food_Poisoning") mInjuryLevel = 6;  // "Nahrungsmittelvergiftung"
		else if (Injury == "Alcohol_Poisoning") mInjuryLevel = 7;  // "Alkoholvergiftung"
		else if (Injury == "Low_Blood_Pressure") mInjuryLevel = 6;  // "Niedriger Bludruck, Patient zusammengebrochen"
		else if (Injury == "Angina_Pectoris") mInjuryLevel = 4;  // "Angina Pectoris"
		else if (Injury == "Shortage_Of_Breath") mInjuryLevel = 3;  // "Starke Atemnot durch allergische Reaktion"
		else if (Injury == "Collapsed_lung") mInjuryLevel = 3; // "Lungenriss"
		else if (Injury == "Hypothermia") mInjuryLevel = 0; // "Schwere Unterkühlung, Kreislaufstillstand"
		else if (Injury == "Avian_Influenza") mInjuryLevel = 9;  // "Vogelgrippe (Influenza): Patient hat Lebensenergie, steht"
		else if (Injury == "Avian_Influenza_Injured") mInjuryLevel = 9;  // "Vogelgrippe (Influenza): Patient hat Verletztenenergie, liegt"
		else if (Injury == "Swine_Influenza") mInjuryLevel = 9;  // "Schweinegrippe: Patient hat Lebensenergie, steht"
		else if (Injury == "Swine_Influenza_Injured") mInjuryLevel = 9;  // "Schweinegrippe: Patient hat Verletztenenergie, liegt"	
		else if (Injury == "Hard_Asthma") mInjuryLevel = 9;  // "Schwerer Asthmaanfall: Patient hat Lebensenergie, steht"
		else if (Injury == "Hard_Asthma_Injured") mInjuryLevel = 9;  // "Schwerer Asthmaanfall: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Hydrophobia") mInjuryLevel = 9;  // "Gehirnentzündung durch Tollwut: Patient hat Lebensenergie, steht"
		else if (Injury == "Hydrophobia_Injured") mInjuryLevel = 9;  // "Gehirnentzündung durch Tollwut: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Chemical_Contamination") mInjuryLevel = 9;  // "Chemische Kontamination: Patient hat Lebensenergie, steht"
		else if (Injury == "Chemical_Contamination_Shortage_Of_Breath") mInjuryLevel = 3;  // "Schwere Atemnot wegen chemischer Kontamination: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Chemical_Contamination_Breath_Burn") mInjuryLevel = 3;  // "Verätzung der Atemwege wegen chemischer Kontamination: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Radioactive_Contamination") mInjuryLevel = 9;  // "Radioaktive Kontamination: Patient hat Lebensenergie, steht"
		else if (Injury == "Radioactive_Contamination_Organ_Bleeding") mInjuryLevel = 6;  // "Organblutung als Folge einer radioaktiven Kontamination"
		else if (Injury == "Radioactive_Contamination_Organ_Failure") mInjuryLevel = 0;  // "Organversagen als Folge einer radioaktiven Kontamination "
		else if (Injury == "Plague_Contamination") mInjuryLevel = 9;		// "Pestkrankheit"
		else if (Injury == "Plague_Contamination_Positiv") mInjuryLevel = 9;			// "Pestkrankheit quick test positiv"
		else if (Injury == "Plague_Contamination_Negativ") mInjuryLevel = 9;			// "Pestkrankheit quick test negativ"
		else if (Injury == "Drowning_normal") mInjuryLevel = 0;  // "Ertrinken normal"

		else mInjuryLevel = 10; 
		return mInjuryLevel;

		//
		// 0 = Bewusstlosigkeit
		// 1 = Verletzungen (Blut)
		// 2 = Verletzungen (Knochenbrüche)
		// 3 = Atemwegsprobleme (mit Bewusstsein)
		// 4 = Herzinfarkt (oder ähnliche Krankheiten/Verletzungen)
		// 5 = Amputationen
		// 6 = Zusammengebrochene Person (=/= Bewusstlosikeit)
		// 7 = Alkoholvergiftung
		// 8 = Verletzungen (Verbrennungen)
		// 9 = Kranke Person
		// 10 = Fehler / Unklar
		//
	}

	std::string MedicalTestEvent::setEventNamefromRandom(std::string Injury)
	{
		int mInjuryLevel = MedicalTestEvent::SetInjuryLevel(Injury);
		std::string mEventName;
		
		int mRandom = qsf::Random::getRandomInt(0, 4);

		if (mInjuryLevel == 0)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_01");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_02");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_03");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_04");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_05");
		}
		else if (mInjuryLevel == 1)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_06");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_07");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_08");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_09");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_10");
		}
		else if (mInjuryLevel == 2)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_11");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_12");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_13");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_14");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_15");
		}
		else if (mInjuryLevel == 3)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_16");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_17");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_18");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_19");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_20");
		}
		else if (mInjuryLevel == 4)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_21");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_22");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_23");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_24");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_25");
		}
		else if (mInjuryLevel == 5)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_26");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_27");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_28");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_29");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_30");
		}
		else if (mInjuryLevel == 6)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_31");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_32");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_33");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_34");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_35");
		}
		else if (mInjuryLevel == 7)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_36");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_37");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_38");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_39");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_40");
		}
		else if (mInjuryLevel == 8)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_41");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_42");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_43");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_44");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_45");
		}
		else if (mInjuryLevel == 9)
		{
			if (mRandom == 0) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_46");
			else if (mRandom == 1) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_47");
			else if (mRandom == 2) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_48");
			else if (mRandom == 3) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_49");
			else if (mRandom == 4) mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_50");
		}
		else if (mInjuryLevel == 10)
		{
			mEventName = QT_TR_NOOP("AMS_EVENT_TITEL_51");
		}
		else
		{
			mEventName = "AMS_EVENT_TITEL_ERROR";
		}

		return mEventName;
	}

} //Namespace AMS_Event end